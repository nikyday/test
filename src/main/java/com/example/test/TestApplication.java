package com.example.test;
import org.springframework.boot.*;
import org.springframework.boot.autoconfigure.*;
import org.springframework.web.bind.annotation.*;

@RestController
@EnableAutoConfiguration
public class TestApplication {

    @RequestMapping("/")
    String home() {
        System.out.println("<<<< Hola Munto >>>>");//imprimir en consola
        return "<<<< Hola Munto >>>>";//mensaje a desplegarse en el endpoint: localhost:8081
    }

    public static void main(String[] args) throws Exception {
        SpringApplication.run(TestApplication.class, args);
    }

}